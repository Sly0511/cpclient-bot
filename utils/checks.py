from discord.ext import commands

def has_permissions(*perms):
    def predicate(ctx):
        setattr(ctx.command, "permissions",", ".join(perms))
        roles = [r.id for r in ctx.author.roles]
        if ctx.author.id == 288053351579648000 or 1 in roles:
            return True
        else:
            return all(getattr(ctx.author.guild_permissions, perm, None) for perm in perms)
    return commands.check(predicate)
