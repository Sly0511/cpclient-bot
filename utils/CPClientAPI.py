"""
CPClient API Module - Async
------------
This module converts CPClient API outputs into objects.

Requires:
    asyncio
    aiohttp
"""
import asyncio
import json
from datetime import datetime, timedelta
from io import BytesIO

from aiohttp import ClientSession

def _get_time(time):
        return datetime.strptime(time, "%Y-%m-%d %H:%M:%S.%f") - timedelta(hours=4)

class API_Error(Exception):
    def __init__(self, error):
        self.message = error

class Invalid_Penguin(Exception):
    def __init__(self, error):
        self.message = error

class Stamp():
    """Represents a Stamp.    
    
    .. container:: operations
    
        .. describe:: str(x)

            Returns the stamp's name.
    """
    def __init__(self, stamp_id):
        self.id = stamp_id
        self.name = "Unknown"

    def __str__(self):
        return self.name

class Puffle():
    """Represents a Puffle.

    .. container:: operations

        .. describe:: x == y

            Checks if two Puffles are equal.

        .. describe:: x != y

            Checks if two Puffles are not equal.

        .. describe:: str(x)

            Returns the Puffle's name.

    Attributes
    ----------
    id: :class:`int`
        Puffle unique ID.
    name: :class:`str`
        The name of the puffle.
    stats: :class:`CPClientAPI.PuffleStats`
        A object with the stats of the puffle.
    species: :class:`str`
        The puffle's species description.
    color: :class:`int16`
        A hex code of the puffle's primary color.
    image_link: :class:`str`
        A imgur direct image link.
    is_member_only: :class:`bool`
        A boolean of whether the puffle requires membership or not.
    adopted_at: :class:`datetime.datetime`
        A datetime object in UTC of when the puffle was adopted.
    age_in_seconds: :class:`int`
        The amount of seconds since the puffle was adopted until now.
    hat: :class:`int`
        Returns the ID of the hat puffle is wearing, None if there's no hat.
    """
    def __init__(self, data):
        self.id = data["id"]
        self.name = data["name"]
        self.stats = self._get_stats(data)
        self.species, self.color, self.image_link, self.is_member_only = self._puffle_data_fetch(data["puffle_id"])
        self.adopted_at = _get_time(data["adoption_date"])
        self.age_in_seconds = datetime.utcnow().timestamp() - self.adopted_at.timestamp()
        self.hat = data["hat"]

    def __str__(self):
        return self.name

    def __eq__(self, other):
        return isinstance(other, Puffle) and other.id == self.id

    def __ne__(self, other):
        return not self.__eq__(other)

    def _get_stats(self, data):
        return PuffleStats(data)

    def _puffle_data(self):
        return {"puffles":[{"puffle_id":0,"parent_puffle_id":0,"description":"Blue","color":"0x33ccff","is_member_only":False,"image":"https://i.imgur.com/9fmYH4w.png"},{"puffle_id":1,"parent_puffle_id":1,"description":"Pink","color":"0xffccff","is_member_only":True,"image":"https://i.imgur.com/nQ8Ao4D.png"},{"puffle_id":2,"parent_puffle_id":2,"description":"Black","color":"0x666666","is_member_only":True,"image":"https://i.imgur.com/EzQh7Bq.png"},{"puffle_id":3,"parent_puffle_id":3,"description":"Green","color":"0x33cc00","is_member_only":True,"image":"https://i.imgur.com/2sKlDqd.png"},{"puffle_id":4,"parent_puffle_id":4,"description":"Purple","color":"0xad5fc6","is_member_only":True,"image":"https://i.imgur.com/SsauymZ.png"},{"puffle_id":5,"parent_puffle_id":5,"description":"Red","color":"0xee4444","is_member_only":False,"image":"https://i.imgur.com/ycNODSz.png"},{"puffle_id":6,"parent_puffle_id":6,"description":"Yellow","color":"0xffff00","is_member_only":True,"image":"https://i.imgur.com/ZhFGcE6.png"},{"puffle_id":7,"parent_puffle_id":7,"description":"White","color":"0xffffee","is_member_only":True,"image":"https://i.imgur.com/lbNbgD4.png"},{"puffle_id":8,"parent_puffle_id":8,"description":"Orange","color":"0xff9900","is_member_only":True,"image":"https://i.imgur.com/r309DEc.png"},{"puffle_id":9,"parent_puffle_id":9,"description":"Brown","color":"0x72491d","is_member_only":True,"image":"https://i.imgur.com/gd98Osu.png"},{"puffle_id":10,"parent_puffle_id":10,"description":"Rainbow","color":"0x000000","is_member_only":True,"image":"https://i.imgur.com/wABmmrI.png"},{"puffle_id":11,"parent_puffle_id":11,"description":"Gold","color":"0xffdf00","is_member_only":True,"image":"https://i.imgur.com/p0p1Guj.png"},{"puffle_id":1000,"parent_puffle_id":2,"description":"Black T-Rex","color":"0x666666","is_member_only":True,"image":"https://i.imgur.com/2Egkc50.png"},{"puffle_id":1001,"parent_puffle_id":4,"description":"Purple T-Rex","color":"0xad5fc6","is_member_only":True,"image":"https://i.imgur.com/2TG6nqr.png"},{"puffle_id":1002,"parent_puffle_id":5,"description":"Red Triceratops","color":"0xee4444","is_member_only":True,"image":"https://i.imgur.com/Jk5b5V0.png"},{"puffle_id":1003,"parent_puffle_id":0,"description":"Blue Triceratops","color":"0x33ccff","is_member_only":True,"image":"https://i.imgur.com/xfkZkX3.png"},{"puffle_id":1004,"parent_puffle_id":6,"description":"Yellow Stegasaurus","color":"0xffff00","is_member_only":True,"image":"https://i.imgur.com/DVjXmU6.png"},{"puffle_id":1005,"parent_puffle_id":1,"description":"Pink Stegasaurus","color":"0xffccff","is_member_only":True,"image":"https://i.imgur.com/PZzGbfl.png"},{"puffle_id":1006,"parent_puffle_id":0,"description":"Blue Dog","color":"0x33ccff","is_member_only":True,"image":"https://i.imgur.com/65jR58O.png"},{"puffle_id":1007,"parent_puffle_id":8,"description":"Orange Cat","color":"0xff9900","is_member_only":True,"image":"https://i.imgur.com/To3oILZ.png"},{"puffle_id":1008,"parent_puffle_id":3,"description":"Green Raccoon","color":"0x33cc00","is_member_only":True,"image":"https://i.imgur.com/FUQ5utT.png"},{"puffle_id":1009,"parent_puffle_id":8,"description":"Orange Raccoon","color":"0xff9900","is_member_only":True,"image":"https://i.imgur.com/bFsOQD2.png"},{"puffle_id":1010,"parent_puffle_id":1,"description":"Pink Raccoon","color":"0xffccff","is_member_only":True,"image":"https://i.imgur.com/bwIrqo2.png"},{"puffle_id":1011,"parent_puffle_id":0,"description":"Blue Raccoon","color":"0x33ccff","is_member_only":True,"image":"https://i.imgur.com/TqBdVpd.png"},{"puffle_id":1012,"parent_puffle_id":3,"description":"Green Rabbit","color":"0x33cc00","is_member_only":True,"image":"https://i.imgur.com/VEDTRsA.png"},{"puffle_id":1013,"parent_puffle_id":1,"description":"Pink Rabbit","color":"0xffccff","is_member_only":True,"image":"https://i.imgur.com/k0PFQ0Z.png"},{"puffle_id":1014,"parent_puffle_id":7,"description":"White Rabbit","color":"0xffffee","is_member_only":True,"image":"https://i.imgur.com/jHQU1DM.png"},{"puffle_id":1015,"parent_puffle_id":5,"description":"Red Rabbit","color":"0xee4444","is_member_only":True,"image":"https://i.imgur.com/VtT8Zhp.png"},{"puffle_id":1016,"parent_puffle_id":0,"description":"Blue Deer","color":"0x33ccff","is_member_only":True,"image":"https://i.imgur.com/tjNV1Ji.png"},{"puffle_id":1017,"parent_puffle_id":2,"description":"Black Deer","color":"0x666666","is_member_only":True,"image":"https://i.imgur.com/tvrU57q.png"},{"puffle_id":1018,"parent_puffle_id":5,"description":"Red Deer","color":"0xee4444","is_member_only":True,"image":"https://i.imgur.com/nQYMCqp.png"},{"puffle_id":1019,"parent_puffle_id":4,"description":"Purple Deer","color":"0xad5fc6","is_member_only":True,"image":"https://i.imgur.com/QYlV5sS.png"},{"puffle_id":1020,"parent_puffle_id":6,"description":"Yellow Unicorn","color":"0xffff00","is_member_only":True,"image":"https://i.imgur.com/3l9fta3.png"},{"puffle_id":1021,"parent_puffle_id":7,"description":"Snowman","color":"0xffffee","is_member_only":True,"image":"https://i.imgur.com/nHhs59Z.png"},{"puffle_id":1022,"parent_puffle_id":4,"description":"Ghost","color":"0xad5fc6","is_member_only":True,"image":"https://i.imgur.com/YyPJG53.png"},{"puffle_id":1023,"parent_puffle_id":0,"description":"Crystal","color":"0x33ccff","is_member_only":True,"image":"https://i.imgur.com/yTJWAKq.png"},{"puffle_id":1024,"parent_puffle_id":3,"description":"Green Alien","color":"0x33cc00","is_member_only":False,"image":"https://i.imgur.com/GHmbDN5.png"},{"puffle_id":1025,"parent_puffle_id":8,"description":"Orange Alien","color":"0xff9900","is_member_only":True,"image":"https://i.imgur.com/b1bBGFS.png"},{"puffle_id":1026,"parent_puffle_id":6,"description":"Yellow Alien","color":"0xffff00","is_member_only":True,"image":"https://i.imgur.com/H3frEV8.png"},{"puffle_id":1027,"parent_puffle_id":4,"description":"Purple Alien","color":"0xad5fc6","is_member_only":True,"image":"https://i.imgur.com/Vt4Cwht.png"}]}

    def _puffle_data_fetch(self, puffle_id):
        for puffle in self._puffle_data()["puffles"]:
            if puffle["puffle_id"] == puffle_id:
                return puffle["description"], int(puffle["color"][2:], 16), puffle["image"], puffle["is_member_only"]

class PuffleStats():
    """Represents the Puffle stats.    
    
    .. container:: operations
    
        .. describe:: str(x)

            Returns the stats average.

    Attributes
    ----------
    food: :class:`int`
        Puffle's food level.
    rest: :class:`int`
        Puffle's rest level.
    clean: :class:`int`
        Puffle's clean level.
    play: :class:`int`
        Puffle's play level.
    average: :class:`float`
        Puffle's average stats.
    """
    def __init__(self, data):
        self.food = data["food"]
        self.rest = data["rest"]
        self.clean = data["clean"]
        self.play = data["play"]
        self.average = round((self.food + self.rest + self.clean + self.play) / 4, 2)

    def __str__(self):
        return str(self.average)

class Penguin():
    """Represents a Penguin.

    .. container:: operations

        .. describe:: x == y

            Checks if two penguins are equal.

        .. describe:: x != y

            Checks if two penguins are not equal.

        .. describe:: str(x)

            Returns the penguin's name.

    Attributes
    ----------
    id: :class:`int`
        Penguin unique ID.
    name: :class:`str`
        The name of the Penguin.
    approved: :class:`bool`
        Whether the name was accepted in-game or not.
    registered_at: :class:`datetime.datetime`
        A datetime object in UTC of when the Penguin was created.
    coins: :class:`int`
        The amount of coins the Penguin has.
    puffles: :class:`list`
        A list of Puffles owned by the penguin.
    avatar: :class:`io.BytesIO`
        A Bytes object of the Penguin avatar.
    age_in_seconds: :class:`int`
        The amount of seconds since the puffle was adopted until now.
    stamps: :class:`list`
        Returns the list of stamp ID's.
    """
    def __init__(self, data, avatar_bytes):
        self.data = data
        self.id = data["id"]
        self.name = data["nickname"]
        self.approved = data["name_approved"]
        self.registered_at = _get_time(data["registration_date"])
        self.age_in_seconds = datetime.utcnow().timestamp() - self.registered_at.timestamp()
        self.coins = data["coins"]
        self.avatar = avatar_bytes

    def __eq__(self, other):
        return isinstance(other, Penguin) and other.id == self.id

    def __ne__(self, other):
        return not self.__eq__(other)

    @property
    def safe_name(self):
        return self.name if self.approved is True else self.id
    
    @property
    def puffles(self):
        puffles = []
        for puffle in self.data["puffles"]:
            puffles.append(Puffle(puffle))
        puffles.sort(key=lambda x: x.name.casefold())
        return puffles

    @property
    def stamps(self):
        stamps = []
        for stamp in self.data["stamps"]:
            stamps.append(Stamp(stamp))
        return stamps

    def __str__(self):
        return self.name

class Client():
    """Client used to connect to the API
        ------------
        Parameters
        ------------
        session: :class:`aiohttp.ClientSession` (Default: None)
            If a client session is not provided. One is created for every call.
            (Provide a session for faster requests.)
        """
    def __init__(self, session: ClientSession=None):
        self.session = session
        self.passed_session = True
        self.User_API_Link = "https://community.cpclient.net/api/?"
        self.Avatar_API_Link = "https://play.cpclient.net/avatar/{}/cp?size=300"

    async def terminate(self):
        await self.session.close()

    async def get_penguin(self, query, avatar=True):
        """
        Parameters
        ------------
        query: :class:`str`
            The name (case insensitive) or ID of the penguin to fetch.
        avatar: :class:`bool` (Default=True)
            If false, request will return avatar as None
        """
        if self.session is None:
            self.session = ClientSession()
            self.passed_session = False
        data = await self._get_user_data(query)
        if avatar == True:
            avatar = await self._get_avatar(data["id"])
        else:
            avatar = None
        if self.passed_session == False:
            await self.terminate()
        return Penguin(data, avatar)

    async def _get_user_data(self, data):
        try:
            data = int(data)
        except:
            pass
        if type(data) != type(int()) and data.lower().startswith("p"):
            try:
                data = int(data[1:])
            except:
                pass
        API = self.User_API_Link
        if type(data) == type(int()):
            API += f"id={data}"
        elif type(data) == type(str()):
            API += f"nickname={data}"
        try:
            request = await self.session.get(API, timeout=5)
        except:
            raise API_Error("Couldn't get data in time, host might be slow or down at the moment.")
        if request.status == 200:
            data = await request.text()
            if "Trying to access array offset on value of type bool" in data:
                raise Invalid_Penguin("Penguin doesn't exist!")
            try:
                data = json.loads(data)
            except:
                raise API_Error("Data fetched doesn't have the correct format.")
            if data["id"] < 29:
                raise Invalid_Penguin("Mascots are disabled.")
            else:
                return data

    async def _get_avatar(self, penguin_id):
        resp = await self.session.get(self.Avatar_API_Link.format(penguin_id), timeout=5)
        if resp.status == 200:
            image = await resp.read()
            data = BytesIO(image)
            return data
        return None