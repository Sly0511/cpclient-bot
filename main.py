import asyncio
import os
import sys
import traceback
from datetime import datetime

import discord
import utils.checks as perms
from discord.ext import commands
from pymongo import MongoClient

PREFIX = ">"
TOKEN = ""

bot = commands.AutoShardedBot(command_prefix=PREFIX, case_insensitive=True, description='CPC Bot', pm_help=None, status=discord.Status.dnd, activity=discord.Game(f"Starting up..."))
bot.remove_command('help')
bot.admin_ids = []

def db_check():
    settings = bot.db.settings.find_one({"_id": bot.user.id})
    update = {}
    if not settings:
        update["allowed_channels"] = []
        update["cpcd_logs"] = None
        update["word_filter"] = []
        update["word_logs"] = None
        bot.db.settings.update_one({"_id": bot.user.id}, {"$set": update}, upsert=True)
        return
    if "allowed_channels" not in settings:
        update["allowed_channels"] = []
    if "cpcd_logs" not in settings:
        update["cpcd_logs"] = None
    if "word_filter" not in settings:
        update["word_filter"] = []
    if "word_logs" not in settings:
        update["word_logs"] = None
    if update != {}:
        bot.db.settings.update_one({"_id": bot.user.id}, {"$set": update})

@bot.event
async def on_ready():
    sys.stdout.write('\rFinished Loading!  \n')
    print("===================================================")
    bot.db_client = MongoClient()
    bot.db = bot.db_client["CPC"]
    db_check()
    print("Database connection successful!")
    print("===================================================")
    setattr(bot, "uptime", datetime.utcnow().timestamp())
    setattr(bot, "_last_exception", None)
    await bot.change_presence(status=discord.Status.online, activity=discord.Game("CPClient.net"))
    try:
        bot.load_extension("cogs.owner")
    except Exception as e:
        print(f'{e}')

@bot.event
async def on_message_edit(before, after):
    if after.author.bot:
        return
    elif isinstance(after.channel, discord.abc.PrivateChannel) and after.content.startswith(PREFIX):
        e=discord.Embed(description="Commands are not available in direct messages!", colour=0xffff00)
        await after.channel.send(embed=e)
    #else:
    #    settings = bot.db.settings.find_one({"_id": bot.user.id})
    #    if after.guild.id == 715212041887023204 and after.channel.id not in settings["allowed_channels"] and not after.author.guild_permissions.ban_members:
    #        return
        #await bot.process_commands(after)

@bot.event
async def on_message(message):
    if message.author.bot:
        return
    if isinstance(message.channel, discord.abc.PrivateChannel) and message.content.startswith(PREFIX):
        e=discord.Embed(description="Commands are not available in direct messages!", colour=0xffff00)
        await message.channel.send(embed=e)
    #else:
    #    settings = bot.db.settings.find_one({"_id": bot.user.id})
    #    if message.guild.id == 715212041887023204 and message.channel.id not in settings["allowed_channels"] and not message.author.guild_permissions.ban_members:
    #        return
        #await bot.process_commands(message)

@bot.event
async def on_command(ctx):
    print(f"Command Handler\n    Author ID: {ctx.author.id}\n    Guild: {ctx.guild.name} [ {ctx.guild.id} ]\n    Channel: [ {ctx.channel.id} ]\n    Command: {ctx.message.content}")

@bot.event
async def on_command_error(ctx, error, *args, **kwargs):
    if not isinstance(error, commands.CommandNotFound):
        exception_log = "Exception in command '{}' - {}\n" "".format(ctx.command.qualified_name, ctx.command.cog_name)
        exception_log += "".join(traceback.format_exception(type(error), error, error.__traceback__))
        bot._last_exception = exception_log
    channel = ctx.channel
    if isinstance(error, commands.CheckFailure):
        e=discord.Embed(description="You are missing permissions to use this command!", colour=0xffff00)
        return await channel.send(embed=e)
    elif isinstance(error, commands.NoPrivateMessage):
        e=discord.Embed(description="Commands are not available in direct messages!", colour=0xffff00)
        return await channel.send(embed=e)
    elif isinstance(error, commands.DisabledCommand):
        e=discord.Embed(description=f"**{ctx.command}** is currently disabled!", colour=0xffff00)
        return await channel.send(embed=e)
    elif isinstance(error, commands.CommandOnCooldown):
        m, s = divmod(error.retry_after, 60)
        h, m = divmod(m, 60)
        if h == 0:
            time = "%d minutes %d seconds" % (m, s)
        elif h == 0 and m == 0:
            time = "%d seconds" % (s)
        else:
            time = "%d hours %d minutes %d seconds" % (h, m, s)
        try:
            await ctx.message.delete()
        except:
            pass
        return await channel.send("{} This command is on cooldown! Try again in {}".format(ctx.author.mention, time), delete_after=5)
    elif isinstance(error, commands.MissingRequiredArgument):
        msg = ""
        for x in ctx.command.params:
            if x != "ctx":
                if x != "self":
                    if "None" in str(ctx.command.params[x]):
                        msg += "[{}] ".format(x)
                    else:
                        msg += "<{}> ".format(x)
        e=discord.Embed(title="Example", description="{}{} {}".format(ctx.prefix, ctx.command, msg), colour=0xffff00)
        e.set_author(name="{}".format(ctx.command), icon_url=bot.user.avatar_url)
        e.set_footer(text="{}".format(ctx.command.help))
        await channel.send(embed=e)
    elif isinstance(error, commands.CommandNotFound):
        pass
    elif isinstance(error, commands.BadArgument):
        msg = ""
        for x in ctx.command.params:
            if x != "ctx":
                if x != "self":
                    if "None" in str(ctx.command.params[x]):
                        msg += "[{}] ".format(x)
                    else:
                        msg += "<{}> ".format(x)
        e=discord.Embed(title="Example", description="{}{} {}".format(ctx.prefix, ctx.command, msg), colour=0xffff00)
        e.set_author(name="{}".format(ctx.command), icon_url=bot.user.avatar_url)
        e.set_footer(text="{}".format(ctx.command.help))
        await channel.send(embed=e)
    elif isinstance(error, commands.CommandInvokeError):
        if "is already loaded" in str(error):
            e = str(error).replace("Command raised an exception: ExtensionAlreadyLoaded: Extension ", "").replace("'", "").replace("cogs.", "").replace(" is already loaded.","")
            er = e.capitalize()
            err = er.replace(er, f"**{er}** already loaded.")
            e=discord.Embed(description=err, colour=0x800000)
            await ctx.send(embed=e)
            await asyncio.sleep(5)
        elif "has not been loaded." in str(error):
            e = str(error).replace("Command raised an exception: ExtensionNotLoaded: Extension ", "").replace("'", "").replace("cogs.", "").replace(" has not been loaded.","")
            er = e.capitalize()
            err = er.replace(er, f"**{er}** is not loaded.")
            e=discord.Embed(description=err, colour=0x800000)
            await ctx.send(embed=e)
            await asyncio.sleep(5)
        elif "has not been loaded." in str(error):
            e = str(error).replace("Command raised an exception: ExtensionNotLoaded: Extension ", "").replace("'", "").replace("cogs.", "").replace(" has not been loaded.","")
            er = e.capitalize()
            err = er.replace(er, f"**{er}** is not loaded.")
            e=discord.Embed(description=err, colour=0x800000)
            await ctx.send(embed=e)
            await asyncio.sleep(5)
        else:
            text = str(error).replace("Command raised an exception: NotFound: 404 NOT FOUND (error code: 10008): Unknown Message", "Message got deleted.")
            if 1 == 1:#"bot.py" in error.__traceback__.tb_frame:
                e=discord.Embed(description="Reason: {}".format(text), colour=0xffff00)
            else:
                e=discord.Embed(description="Name: {}\nLine: {}\nReason: {}".format(error.__class__.__name__, error.__traceback__.tb_lineno, text), colour=0xffff00)
            e.set_author(name="Command Error!", icon_url=bot.user.avatar_url)
            await channel.send(embed=e)

#Connection Status
@bot.event
async def on_connect():
    print("===================================================")
    print("Bot connected as " + bot.user.name + "#" + bot.user.discriminator)
    info = await bot.application_info()
    print("Owner: " + str(info.owner.name) + "#" + str(info.owner.discriminator) + " [" + str(info.owner.id) + "]")
    print("===================================================")
    await asyncio.sleep(1)
    sys.stdout.write("\rLoading commands...")

@bot.command(name="channel")
@perms.has_permissions("administrator")
async def _channel(ctx, channel: discord.TextChannel=None):
    settings = bot.db.settings.find_one({"_id": bot.user.id})["allowed_channels"]
    if ctx.channel.id not in settings:
        settings.append(channel.id)
        bot.db.settings.update_one({"_id": bot.user.id}, {"$set": {"allowed_channels": settings}})
        await ctx.send(f"Added {channel.mention} to the channel whitelist.")
    else:
        settings.remove(channel.id)
        bot.db.settings.update_one({"_id": bot.user.id}, {"$set": {"allowed_channels": settings}})
        await ctx.send(f"Removed {channel.mention} from the channel whitelist.")

@bot.command()
async def restart(ctx):
    if ctx.author.id in bot.admin_ids:
        await ctx.send("Restarting!")
        await bot.change_presence(status=discord.Status.offline)
        os.system("python3.8 main.py")
        os._exit(-1)

bot.run(TOKEN, reconnect=True)
