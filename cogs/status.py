import asyncio
import socket
from datetime import datetime
from io import BytesIO
from json import dumps, loads

import discord
from aiohttp import ClientSession
from discord.ext import commands, tasks
from googletrans import Translator
from iso639 import languages
from PIL import Image, ImageDraw, ImageFont
from xmltodict import parse


# Little Object for us to use as server data, looks prettier than dicts
class Server():
    def __init__(self, data, locale):
        self.id = data["@id"]
        self.name = data["@name"]
        self.address = data["@address"]
        self.port = data["@port"]
        self.safe = data["@safe"]
        self.locale = locale[0]
        self.language = locale[1]


class Status(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.session = ClientSession()
        self.translate = Translator().translate
        self.blacklist = ["3102", "3103"]
        self.login_server = None
        self.servers = []
        # Server Constants
        self.cpclient_server = self.bot.get_guild(715212041887023204)
        self.role = self.cpclient_server.get_role(730644605808279562)
        self.channel = self.cpclient_server.get_channel(730644092932980797)
        # self.cpclient_server = self.bot.get_guild(729596930279211010)
        # self.role = self.cpclient_server.get_role(738434324939931700)
        # self.channel = self.cpclient_server.get_channel(738411849577529344)
        # Constants
        self.servers_list_xml = "https://play.cpclient.net/servers.xml"
        self.offline_color = 0x000000
        self.online_color = 0x000000
        # Tasks
        self.check_server_list.start()  # pylint: disable=no-member
        self.ping_servers.start()  # pylint: disable=no-member

    def cog_unload(self):
        self.check_server_list.cancel()  # pylint: disable=no-member
        self.ping_servers.cancel()  # pylint: disable=no-member
        asyncio.create_task(self.terminate_session())

    async def terminate_session(self):
        await self.session.close()

    def dabatase_setup(self):
        data = self.bot.db.status_ping.find_one({"_id": self.bot.user.id})
        if not data or "servers" not in data.keys():
            data = {"servers": {}, "messages": {}}
        for server in self.servers:
            if server.id not in data["servers"].keys():
                data["servers"][server.id] = False
            if server.id not in data["messages"]:
                data["messages"][server.id] = 0
        self.bot.db.status_ping.update_one(
            {"_id": self.bot.user.id},
            {"$set": {
                "messages": data["messages"],
                "servers": data["servers"]
                }}, upsert=True)

    @tasks.loop(seconds=600)
    async def check_server_list(self):
        servers_list = await self.session.get(self.servers_list_xml)
        if servers_list.status == 200:
            result = await servers_list.text()
            servers_list_dict = loads(dumps(parse(result)))
            servers_list = servers_list_dict["servers"]["environment"]
            # Yep big fat XML to Json converter
            # ------------------------------------
            # Login and redemption don't follow general server rule
            # Custom setup for them
            login_server = {
                "@id": "login",
                "@name": "Login",
                "@address": servers_list["login"]["@address"],
                "@port": servers_list["login"]["@port"],
                "@safe": False
            }
            redeem_server = {
                "@id": "redeem",
                "@name": "Redeem",
                "@address": servers_list["redemption"]["@address"],
                "@port": servers_list["redemption"]["@port"],
                "@safe": False
            }
            # Don't loop add existing server, massive spam vibes
            # Clear and update
            if len(self.servers) > 0:
                self.servers.clear()
            self.servers.append(Server(redeem_server, (None, None)))
            self.servers.append(Server(login_server, (None, None)))
            for server in servers_list["language"]:
                # Not everyone knows what ES FR and PT means
                # Convert it to something more friendly
                lang_name = self.get_language(server["@locale"])
                language = (server["@locale"], lang_name)
            # XML conversion is inconsistent this check should avoid errors
                if isinstance(server["server"], list):
                    for game_server in server["server"]:
                        # Objects are amazing
                        self.servers.append(Server(game_server, language))
                else:
                    # Objects are the best
                    self.servers.append(Server(server["server"], language))
        # Check integrity of database vs servers list
        self.dabatase_setup()

    @tasks.loop(seconds=175)
    async def ping_servers(self):
        # Wait for self.servers to build
        await asyncio.sleep(5)
        # In case something goes horribly wrong
        if len(self.servers) == 0:
            return
        data = self.bot.db.status_ping.find_one({"_id": self.bot.user.id})
        if not data or "servers" not in data.keys():
            return
        now = datetime.utcnow()
        # Setup for later in case mention is needed
        ping = False
        for server in self.servers:
            if server.id in self.blacklist:
                message_id = data["messages"][server.id]
                try:
                    msg = await self.channel.fetch_message(message_id)
                except discord.errors.NotFound:
                    continue
                await msg.delete()
                continue
            # Embed Setup
            embed = discord.Embed(timestamp=now)
            # Check host and port
            result = self.check_ip(server.address, server.port)
            i = 0
            while not result:
                if i == 2:
                    break
                await asyncio.sleep(5)
                result = self.check_ip(server.address, server.port)
                i += 1
            # Check results
            if result:
                embed.color = 0x43b581
                embed.set_thumbnail(url="https://i.imgur.com/ehDEIZa.png")
                embed.add_field(
                    name='Status',
                    value="Online")
                if data["servers"][server.id] is False:
                    data["servers"][server.id] = True
                    ping = True
            if not result:
                embed.color = 0xf04747
                embed.set_thumbnail(url="https://i.imgur.com/2S7msS0.png")
                embed.add_field(
                    name='Status',
                    value="Offline")
                if data["servers"][server.id] is True:
                    data["servers"][server.id] = False
                    ping = True
            # Complete embed
            text = 'Server Status'
            if server.language == "English":
                embed.set_author(
                    name=text,
                    icon_url="https://i.imgur.com/OSKYIq3.png")
            elif server.language == "Spanish":
                embed.set_author(
                    name=text,
                    icon_url="https://i.imgur.com/hiZ4Dpi.png")
            elif server.language == "French":
                embed.set_author(
                    name=text,
                    icon_url="https://i.imgur.com/xye2no3.png")
            elif server.language == "Portuguese":
                embed.set_author(
                    name=text,
                    icon_url="https://i.imgur.com/1YXknmi.png")
            elif server.name == "Redeem":
                embed.set_author(
                    name=text,
                    icon_url="https://i.imgur.com/MIIarEL.png")
            else:
                embed.set_author(
                    name=text,
                    icon_url=self.bot.user.avatar_url)
            if server.language:
                language_name = self.translate(server.language, dest=server.language)
                language = f"{language_name.text}"
                embed.add_field(name="Language", value=language)
            embed.set_image(url="attachment://server_banner.png")
            embed.set_footer(text="Last updated")
            try:
                message_id = data["messages"][server.id]
                msg = await self.channel.fetch_message(message_id)
                if ping:
                    await msg.delete()
                    msg = await self.channel.send(embed=embed, file=self.generate_image(server.name, server.language))
                    data["messages"][server.id] = msg.id
                    continue
                await msg.edit(embed=embed)
            except discord.errors.NotFound:
                # Force ping if the message was manually deleted
                ping = True
                msg = await self.channel.send(embed=embed, file=self.generate_image(server.name, server.language))
                data["messages"][server.id] = msg.id
        # Mention if a change is detected
        if ping:
            await self.channel.send(self.role.mention, delete_after=1)
        # Update database with new data
        self.bot.db.status_ping.update_one(
            {"_id": self.bot.user.id},
            {"$set": {
                "messages": data["messages"],
                "servers": data["servers"]
                }}, upsert=True)

    def get_language(self, locale):
        language = languages.get(part1=locale).name
        return language

    def check_ip(self, ip, port):
        # Works kinda?
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        host = (ip, int(port))
        s.settimeout(3)
        try:
            s.connect(host)
            s.shutdown(socket.SHUT_RDWR)
            s.close()
            return True
        except Exception as e:
            print(str(e))
            try:
                s.close()
            except Exception as e:
                print(str(e))
            return False

    def generate_image(self, text, language):
        width, height = 1000, 200
        font = ImageFont.truetype("data/fonts/burbank.otf", 170)

        bg = Image.open("data/images/bannerbg.png", "r")
        img = Image.new('RGBA', (1000, 200), 0)
        img.paste(bg)
        draw = ImageDraw.Draw(img)
        w, h = draw.textsize(text, font=font)
        
        # if language is not None:
        #     flag = Image.open(f"data/images/{language.lower()}.png", "r")
        #     flag = flag.resize((166, 111), Image.ANTIALIAS)
        #     img.paste(flag, (0, 45))
        #     draw.text((180, (height-h)/2), text, (255,255,255), font=font)
        # else:
        draw.text(((width-w)/2, (height-h)/2), text, (255,255,255), font=font)

        image = BytesIO()
        img.save(image, "PNG")
        image.seek(0)
        return discord.File(image, filename=f"server_banner.png")


def setup(bot):
    bot.add_cog(Status(bot))
