import base64
from datetime import datetime

import aiohttp
import discord
import requests
from discord.ext import commands

from googletrans import Translator
from iso639 import languages


class General(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.translate = Translator().translate

    @commands.command(aliases=["tran", "tr"])
    @commands.cooldown(1, 7, commands.BucketType.user)
    async def translate(self, ctx, language, *, text):
        """Translate one language to another"""
        if len(language) == 2:
            try:
                language = languages.get(part1=language).name
            except:
                pass
        elif len(language) == 3:
            try:
                language = languages.get(part3=language).name
            except:
                pass
        try:
            translated = self.translate(text, dest=language)
        except ValueError:
            await ctx.send("Invalid target language.")
            return
        s=discord.Embed(colour=0x4285f4)
        s.set_author(name="Google Translate", icon_url="https://upload.wikimedia.org/wikipedia/commons/d/db/Google_Translate_Icon.png")
        s.add_field(name="Input Text ({})".format(languages.get(part1=translated.src).name.title()), value=text, inline=False)
        s.add_field(name="Output Text ({})".format(language.title()), value=translated.text)
        await ctx.send(embed=s)

    @commands.command(aliases=["iu"])
    async def imgurupload(self, ctx, link: str=None):
        if ctx.author.id != 288053351579648000:
            return
        try:
            if link != None:
                link = link.replace("<", "").replace(">", "")
            elif not link:
                if ctx.message.attachments:
                    link = ctx.message.attachments[0].url
                else:
                    await ctx.send("Please provide a image direct link or an attachment.")
                    return
            async with aiohttp.ClientSession() as session:
                async with session.get(link) as resp:
                    if resp.status == 200:
                        with open(f'cache.png', 'wb+') as file:
                            file.write(await resp.read())
                    else:
                        try:
                            await ctx.message.attachments[0].save("cache.png")
                        except:
                            await ctx.send("I was unable to get that image.")
                            return
            try:
                await ctx.message.delete()
            except:
                pass
            with open("cache.png", "rb") as f:
                contents = f.read()
                b64 = base64.b64encode(contents)
                data = {
                    'image': b64,
                    'type': 'base64',
                }
            url = 'https://api.imgur.com/3/image'
            headers = {
              'Authorization': 'Client-ID 4f519bf55fa5e52'
            }
            i = 0
            if i == 0:
                i += 1
                response = requests.request('POST', url, headers = headers, data = data, allow_redirects=False)
                try:
                    if response .json()["data"]["code"] == 1003:
                        await ctx.send("File Type invalid")
                        return
                except:
                    pass
                linkerino = response.json()["data"]["link"]
                e=discord.Embed(description=f"You imgur link: {linkerino}", color=ctx.author.color)
                message = await ctx.send(embed=e)
                await message.add_reaction("👁")
                while datetime.utcnow().timestamp() < ctx.message.created_at.timestamp() + 60:
                    def check(reaction, user):
                        return user == ctx.author and str(reaction.emoji) == '👁' and reaction.message.id == message.id
                    try:
                        reaction, user = await self.bot.wait_for('reaction_add', check=check, timeout=60)
                    except:
                        return
                    e=discord.Embed(description=f"You imgur link: {linkerino}", color=ctx.author.color)
                    e.set_image(url=linkerino)
                    await message.edit(embed=e)
                    def check(reaction, user):
                        return user == ctx.author and str(reaction.emoji) == '👁' and reaction.message.id == message.id
                    try:
                        reaction, user = await self.bot.wait_for('reaction_remove', check=check, timeout=60)
                    except:
                        return
                    e=discord.Embed(description=f"You imgur link: {linkerino}", color=ctx.author.color)
                    await message.edit(embed=e)
        except Exception as e:
            await ctx.send("An error ocurred, try again please!")
            print(e)

def setup(bot):
    bot.add_cog(General(bot))
