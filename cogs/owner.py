import json
import os
from datetime import datetime

import discord
from discord.ext import commands
from pymongo import MongoClient

wrap = "```py\n{}\n```"

class Main(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        setattr(bot, "owner", 288053351579648000)
        setattr(bot, "admin_ids", [288053351579648000, 510311294532845615])
        setattr(bot, "comment", 0x000080)
        setattr(bot, "success", 0x008000)
        setattr(bot, "error", 0x800000)
        setattr(bot, "progress", 0xf9d71c)
        self.modules = sorted([x.replace(".py", "") for x in os.listdir("cogs") if ".py" in x if not "owner" in x])
        for m in self.modules:
            try:
                self.bot.load_extension("cogs." + m)
            except:
                pass

    @commands.command(hidden=True, aliases=["uh"])
    async def updatehelp(self, ctx):
        if ctx.author.id not in self.bot.admin_ids:
            return
        help = json.loads(open("data/help.json").read())
        data = []
        for key, value in help.items():
            value["_id"] = key
            data.append(value)
        self.bot.db.help.delete_many({})
        self.bot.db.help.insert_many(data)
        await ctx.send("Successfully updated help!")

    @commands.command(hidden=True, aliases=["runcd"])
    async def parse(self, ctx):
        if ctx.author.id in self.bot.admin_ids:
            code = ctx.message.content[8:]
            code = "    " + code.replace("\n", "\n    ")
            code = "async def __eval_function__():\n" + code
            #Base Variables
            additional = {}
            additional["self"] = self
            additional["feu"] = self.bot.fetch_user
            additional["fem"] = ctx.channel.fetch_message
            additional["dlt"] = ctx.message.delete
            additional["xtracord"] = self.bot.db_client["XtraCord"]
            additional["testing"] = self.bot.db_client["XtraCord-Testing"]
            additional["sly"] = self.bot.db_client["sly"]
            additional["now"] = datetime.utcnow()
            additional["nowts"] = datetime.utcnow().timestamp()
            additional["ctx"] = ctx
            additional["sd"] = ctx.send
            additional["channel"] = ctx.channel
            additional["author"] = ctx.author
            additional["guild"] = ctx.guild
            try:
                exec(code, {**globals(), **additional}, locals())

                await locals()["__eval_function__"]()
            except Exception as e:
                embed = discord.Embed(description=str(e), colour=self.bot.error)
                await ctx.send(embed=embed)

    @commands.command(name="exception", aliases=["error", "lasterror"])
    async def last_exception(self, ctx):
        if ctx.author.id not in self.bot.admin_ids:
            return
        if self.bot._last_exception:
            if len(self.bot._last_exception) > 2000:
                await ctx.author.send(f"```py\n{self.bot._last_exception[:1900]}```")
                await ctx.author.send(f"```py\n{self.bot._last_exception[1900:]}```")
            else:
                await ctx.author.send(f"```py\n{self.bot._last_exception}```")

    @commands.command(aliases=["modules", "mods"])
    async def cogs(self, ctx):
        "Shows all the cogs."
        if ctx.author.id not in self.bot.admin_ids:
            return
        modules = sorted([x.replace(".py", "") for x in os.listdir("cogs") if ".py" in x])
        loaded = [c.__module__.split(".")[-1] for c in self.bot.cogs.values()]
        modulesi = ""
        if modules == []:
            modulesi = "There are no modules!"
        else:
            for module in modules:
                if module in loaded:
                    modulesi += "📥 **" + module.capitalize() + "**\n"
                else:
                    modulesi += "📤 **" + module.capitalize() + "**\n"
        embed=discord.Embed(colour=self.bot.comment, title=f"Modules - {len(modules)}", description=modulesi)
        await ctx.send(embed=embed)

    @commands.command()
    async def load(self, ctx, *, module: str=None):
        """Load a bot module."""
        if ctx.author.id not in self.bot.admin_ids:
            return
        modules = [x.replace(".py", "") for x in os.listdir("cogs") if ".py" in x and "owner" not in x]
        loaded_modules = [c.__module__.split(".")[-1] for c in self.bot.cogs.values()]
        if not module:
            failed_to_load = []
            msg_to_send = ""
            i = 0
            for m in modules:
                if m not in loaded_modules:
                    try:
                        self.bot.load_extension("cogs."+m)
                    except:
                        i += 1
                        failed_to_load.append(m)
            if i == 0:
                await ctx.send("Successfully loaded all the modules.")
            else:
                msg_to_send += "Successfully loaded **{}/{}** modules.\n".format(len(modules)-i, len(modules))

            if failed_to_load:
                msg_to_send += "Failed to load ``{}`` modules".format(", ".join(failed_to_load))

            if msg_to_send != "":
                await ctx.send(msg_to_send)

            return
        m = "cogs." + module
        try:
            if module in modules:
                if module in loaded_modules:
                    await ctx.send("Cog is already loaded!")
                    return
                self.bot.load_extension(m)
                await ctx.send(f"Successfully loaded **{module}**")
            else:
                await ctx.send(f"Couldn't find the module named **{module}**")
        except Exception as e:
            e=discord.Embed(description="Error:" + wrap.format(type(e).name + ': ' + str(e)), colour=self.bot.comment)
            await ctx.send(embed=e)

    @commands.command(aliases=["ul"])
    async def unload(self, ctx, *, module: str=None):
        """unloads a part of the bot."""
        if ctx.author.id not in self.bot.admin_ids:
            return
        loaded_modules = [c.__module__.split(".")[-1] for c in self.bot.cogs.values()]
        modules = [x.replace(".py", "") for x in os.listdir("cogs") if ".py" in x]
        failed_to_load = []
        msg_to_send = ""
        if not module:
            i = 0
            for m in modules:
                if m == "owner":
                    pass
                else:
                    try:
                        self.bot.unload_extension("cogs."+m)
                    except:
                        i += 1
                        failed_to_load.append(m)
            if i == 0:
                await ctx.send("Successfully unloaded all the modules.")
            else:
                msg_to_send += "Successfully unloaded **{}/{}** modules.\n".format(len(modules)-i, len(modules))

            if failed_to_load:
                msg_to_send += "Modules ``{}`` were already unloaded.".format(", ".join(failed_to_load))

            if msg_to_send != "":
                await ctx.send(msg_to_send)
            return
        m = "cogs." + module
        try:
            if module in modules:
                if module == "owner":
                    await ctx.send("Can't unload owner cog.")
                    return
                if module not in loaded_modules:
                    await ctx.send("Cog is already unloaded!")
                    return
                self.bot.unload_extension(m)
                await ctx.send(f"Successfully unloaded **{module}**.")
            else:
                await ctx.send(f"Couldn't find the module named **{module}**.")
        except Exception as e:
            e=discord.Embed(description="Error:" + wrap.format(type(e).name + ': ' + str(e)), colour=self.bot.comment)
            await ctx.send(embed=e)

    @commands.command(aliases=["rl"])
    async def reload(self, ctx, *, module: str=None):
        """Reloads a part of the bot."""
        if ctx.author.id not in self.bot.admin_ids:
            return
        modules = [x.replace(".py", "") for x in os.listdir("cogs") if ".py" in x]
        loaded_modules = [c.__module__.split(".")[-1] for c in self.bot.cogs.values()]
        failed_to_load = []
        msg_to_send = ""
        if not module:
            i = 0
            for m in modules:
                if m in loaded_modules:
                    try:
                        self.bot.unload_extension("cogs."+m)
                    except:
                        pass
                try:
                    self.bot.load_extension("cogs."+m)
                except:
                    i += 1
                    failed_to_load.append(module)
            if i == 0:
                await ctx.send("Successfully reloaded all the modules.")
            else:
                msg_to_send += "Successfully reloaded **{}/{}** modules.\n".format(len(modules)-i, len(modules))

            if failed_to_load:
                msg_to_send += "Failed to reload ``{}`` modules".format(", ".join(failed_to_load))

            if msg_to_send != "":
                await ctx.send(msg_to_send)

            return
        m = "cogs." + module
        try:
            if module in modules:
                if module in loaded_modules:
                    self.bot.unload_extension(m)
                    self.bot.load_extension(m)
                else:
                    self.bot.load_extension(m)
                await ctx.send(f"Successfully reloaded **{module}**.")
            else:
                await ctx.send(f"Couldn't find a module named **{module}**.")
        except Exception as e:
            #await ctx.send("Failed to reload the cog, check logs!")
            print(f'Couldn\'t load {m}\n{type(e).name}: {e}')

def setup(bot):
    bot.add_cog(Main(bot))
