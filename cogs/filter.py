import re

import discord
import utils.checks as perms
from discord.ext import commands


class WordFilter(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.db = self.bot.db["settings"]

    @commands.Cog.listener("on_message_edit")
    async def edit_filter(self, before, after):
        if after.author.id == self.bot.user.id:
            return
        #if after.author.guild_permissions.ban_members:
        #    await self.bot.process_commands(after)
        #    return
        settings = self.db.find_one({"_id": self.bot.user.id})
        if after.guild.id == 715212041887023204 and after.channel.id not in settings["allowed_channels"] and not after.author.guild_permissions.ban_members:
            return
        bad_words = settings["word_filter"]
        for bad_word in bad_words:
            if re.search(bad_word, after.content, re.IGNORECASE) != None:
                #try:
                #    await after.delete()
                #except:
                #    pass
                #await after.channel.send(f"Hey {after.author.mention}, don't use words like that!")
                if not settings["word_logs"]:
                    return
                channel = after.guild.get_channel(settings["word_logs"])
                if channel:
                    await channel.send(f"{after.author.mention} said a bad word. Trigger: `{re.search(bad_word, after.content, re.IGNORECASE).group(0)}`")
                return
        await self.bot.process_commands(after)

    @commands.Cog.listener("on_message")
    async def filter(self, message):
        if message.author.id == self.bot.user.id:
            return
        #if message.author.guild_permissions.ban_members:
        #    await self.bot.process_commands(message)
        #    return
        settings = self.db.find_one({"_id": self.bot.user.id})
        if message.author.id != 288053351579648000 and message.guild.id == 715212041887023204 and message.channel.id not in settings["allowed_channels"] and not message.author.guild_permissions.ban_members:
            return
        bad_words = settings["word_filter"]
        for bad_word in bad_words:
            if re.search(bad_word, message.content, re.IGNORECASE) != None:
                #try:
                #    await message.delete()
                #except:
                #    pass
                #await message.channel.send(f"Hey {message.author.mention}, don't use words like that!")
                if not settings["word_logs"]:
                    return
                channel = message.guild.get_channel(settings["word_logs"])
                if channel:
                    await channel.send(f"{message.author.mention} said a bad word. Trigger: `{re.search(bad_word, message.content, re.IGNORECASE).group(0)}`")
                return
        await self.bot.process_commands(message)

    @commands.command(name="add")
    @perms.has_permissions("ban_members")
    async def _add(self, ctx, *, arg):
        bad_words = self.db.find_one({"_id": self.bot.user.id})["word_filter"]
        if arg not in bad_words:
            bad_words.append(arg)
            self.db.update_one({"_id": self.bot.user.id}, {"$set": {"word_filter": bad_words}})
            await ctx.send("Word was added.")
        else:
            await ctx.send("That word is already being filtered.")

    @commands.command(name="remove")
    @perms.has_permissions("ban_members")
    async def _remove(self, ctx, *, arg):
        bad_words = self.db.find_one({"_id": self.bot.user.id})["word_filter"]
        if arg in bad_words:
            bad_words.remove(arg)
            self.db.update_one({"_id": self.bot.user.id}, {"$set": {"word_filter": bad_words}})
            await ctx.send("Word was removed.")
        else:
            await ctx.send("That word is not filtered.")

    @commands.command(name="wordlist")
    @perms.has_permissions("ban_members")
    async def _word_list(self, ctx):
        bad_words = self.db.find_one({"_id": self.bot.user.id})["word_filter"]
        text = ""
        for bad_word in bad_words:
            text += f"{bad_word}\n"
        await ctx.send(embed=discord.Embed(title="Filtered words", description=text, color=self.bot.comment))

    @commands.command(name="logwords")
    @perms.has_permissions("administrator")
    async def _log_words(self, ctx, channel: discord.TextChannel):
        settings = self.bot.db.settings.find_one({"_id": self.bot.user.id})
        if not channel:
            if not settings["word_logs"]:
                await ctx.send("You haven't setup a channel!")
                return
            self.bot.db.settings.find_one({"_id": self.bot.user.id}, {"$set": {"word_logs": None}})
            await ctx.send("Channel reset! Bad words won't be logged.")
            return
        if settings["word_logs"] == channel.id:
            await ctx.send("That channel is already set as logs channel.")
            return
        self.bot.db.settings.update_one({"_id": self.bot.user.id}, {"$set": {"word_logs": channel.id}})
        await ctx.send(f"{channel.mention} was set as bad word logs channel.")

def setup(bot):
    bot.add_cog(WordFilter(bot))
