import re

import discord
from discord.ext import commands


class Suggestions(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.suggestion_color = 0x0390fc
        self.approved_color = 0x25de1f
        self.denied_color = 0xf52c2c
        self.suggestion_emotes = ["✅", "❌", "🗑️", "👁️"]

    def database_check(self, server):
        settings = self.bot.db.settings.find_one({"_id": server.id})
        update = {}
        if not settings:
            settings = {}
        if "suggest" not in settings:
            update["suggest"] = {}
            update["suggest"]["posts"] = None
            update["suggest"]["approved"] = None
            update["suggest"]["denied"] = None
        else:
            if "posts" not in settings["suggest"]:
                settings["suggest"]["posts"] = None
                update = settings
            if "approved" not in settings["suggest"]:
                settings["suggest"]["approved"] = None
                update = settings
            if "denied" not in settings["suggest"]:
                settings["suggest"]["denied"] = None
                update = settings
        if update != {}:
            self.bot.db.settings.update_one({"_id": server.id}, {"$set": update}, upsert=True)

    @commands.group(name="suggest_channel", aliases=["suggestchannel", "sc"])
    async def _suggest_channel(self, ctx):
        self.database_check(ctx.guild)
        if ctx.invoked_subcommand == None:
            return
            # Show Help

    @_suggest_channel.command(name="posts")
    async def _posts(self, ctx, channel: discord.TextChannel):
        if not channel:
            self.bot.db.settings.update_one({"_id": ctx.guild.id}, {"$set": {"suggest.posts": None}})
            await ctx.send(f"Reset **posts** channel.")
        else:
            self.bot.db.settings.update_one({"_id": ctx.guild.id}, {"$set": {"suggest.posts": channel.id}})
            await ctx.send(f"{channel.mention} set as **posts** channel.")

    @_suggest_channel.command(name="approved")
    async def _approved(self, ctx, channel: discord.TextChannel):
        if not channel:
            self.bot.db.settings.update_one({"_id": ctx.guild.id}, {"$set": {"suggest.approved": None}})
            await ctx.send(f"Reset **approved** channel.")
        else:
            self.bot.db.settings.update_one({"_id": ctx.guild.id}, {"$set": {"suggest.approved": channel.id}})
            await ctx.send(f"{channel.mention} set as **approved** channel.")

    @_suggest_channel.command(name="denied")
    async def _denied(self, ctx, channel: discord.TextChannel):
        if not channel:
            self.bot.db.settings.update_one({"_id": ctx.guild.id}, {"$set": {"suggest.denied": None}})
            await ctx.send(f"Reset **denied** channel.")
        else:
            self.bot.db.settings.update_one({"_id": ctx.guild.id}, {"$set": {"suggest.denied": channel.id}})
            await ctx.send(f"{channel.mention} set as **denied** channel.")

    @commands.command(name="suggest")
    async def _suggest(self, ctx, *, suggestion):
        self.database_check(ctx.guild)
        settings = self.bot.db.settings.find_one({"_id": ctx.guild.id})
        if settings["suggest"]["posts"] == None:
            await ctx.send("Suggestion wasn't setup yet, use `>sc posts #channel` to do so.")
            return
        channel = self.bot.get_channel(settings["suggest"]["posts"])
        if not channel:
            await ctx.send("Suggestions channel is no longer valid. Please set a new one.")
            return
        e = discord.Embed(title="Suggestions", description=suggestion, color=self.suggestion_color, timestamp=ctx.message.created_at)
        e.set_footer(text=f"From: {ctx.author} | ({ctx.author.id})", icon_url=ctx.author.avatar_url)
        url = None
        if ctx.message.attachments:
            formats = [".png", ".jpg", ".jpeg"]
            url = str(ctx.message.attachments[0].url)
            regex = re.search(r"\?.+", url)
            if regex:
                url = url.replace(regex.group(0), "")
                print(url)
            for formatting in formats:
                if url.endswith(formatting):
                    url = url.replace(formatting, "")
                    e.set_image(url=url)
        msg = await channel.send(embed=e)
        emotes = self.suggestion_emotes
        if url == None:
            emotes = self.suggestion_emotes[:-1]
        for emote in emotes:
            await msg.add_reaction(emote)
        e = discord.Embed(title="Suggestion Sent", description="✅ Your suggestion has successfully been sent", color=0x25de1f)
        await ctx.send(embed=e)

    @commands.group(name="suggestion")
    async def _suggestion(self, ctx):
        self.database_check(ctx.guild)
        if ctx.invoked_subcommand == None:
            return
            # Show Help

    @_suggestion.command(name="approve")
    async def _approve(self, ctx, message_id: int):
        settings = self.bot.db.settings.find_one({"_id": ctx.guild.id})
        if settings["suggest"]["posts"] == None:
            await ctx.send("Suggestion wasn't setup yet, use `>sc posts #channel` to do so.")
            return
        if settings["suggest"]["approved"] == None:
            await ctx.send("Suggestion wasn't setup yet, use `>sc approved #channel` to do so.")
            return
        channel = self.bot.get_channel(settings["suggest"]["posts"])
        if not channel:
            await ctx.send("Suggestions is no longer valid. Please set a new one.")
            return
        msg = await channel.fetch_message(message_id)
        if not msg or not msg.embeds:
            await ctx.send("That is not a valid suggestion message ID.")
            return
        e = msg.embeds[0]
        e.title = "Suggestions | Approved"
        e.description = f"[Jump to original]({msg.jump_url})\n\n" + e.description
        e.color = self.approved_color
        channel = self.bot.get_channel(settings["suggest"]["approved"])
        await channel.send(embed=e)

    @_suggestion.command(name="deny")
    async def _deny(self, ctx, message_id: int):
        settings = self.bot.db.settings.find_one({"_id": ctx.guild.id})
        if settings["suggest"]["posts"] == None:
            await ctx.send("Suggestion wasn't setup yet, use `>sc posts #channel` to do so.")
            return
        if settings["suggest"]["denied"] == None:
            await ctx.send("Suggestion wasn't setup yet, use `>sc denied #channel` to do so.")
            return
        channel = self.bot.get_channel(settings["suggest"]["posts"])
        if not channel:
            await ctx.send("Suggestions is no longer valid. Please set a new one.")
            return
        msg = await channel.fetch_message(message_id)
        if not msg or not msg.embeds:
            await ctx.send("That is not a valid suggestion message ID.")
            return
        e = msg.embeds[0]
        e.title = "Suggestions | Denied"
        e.description = f"[Jump to original]({msg.jump_url})\n\n" + e.description
        e.color = self.denied_color
        channel = self.bot.get_channel(settings["suggest"]["denied"])
        await channel.send(embed=e)

    @commands.Cog.listener("on_raw_reaction_add")
    async def _reaction_management(self, payload):
        # Checks
        if payload.member.id == self.bot.user.id:
            return
        settings = self.bot.db.settings.find_one({"_id": payload.guild_id})
        if not settings:
            return
        if settings["suggest"]["posts"] == None:
            return
        if payload.channel_id != settings["suggest"]["posts"]:
            return
        channel = self.bot.get_channel(settings["suggest"]["posts"])
        if not channel:
            return
        # Clean Emotes
        msg = await channel.fetch_message(payload.message_id)
        emotes = self.suggestion_emotes
        if msg.embeds and not msg.embeds[0].image:
            emotes = self.suggestion_emotes[:-1]
        if str(payload.emoji) not in emotes:
            try:
                await msg.remove_reaction(str(payload.emoji), payload.member)
            except:
                pass
            return
        # Toggle Image
        if str(payload.emoji) == self.suggestion_emotes[3]:
            if not payload.member.guild_permissions.ban_members:
                try:
                    await msg.remove_reaction(str(payload.emoji), payload.member)
                except:
                    pass
                return
            if msg.embeds and msg.embeds[0].image:
                e = msg.embeds[0]
                if str(e.image.url).endswith(".png"):
                    new_url = str(e.image.url).replace(".png", "")
                else:
                    new_url = str(e.image.url) + ".png"
                e.set_image(url=new_url)
                await msg.edit(embed=e)
                try:
                    await msg.remove_reaction(str(payload.emoji), payload.member)
                except:
                    pass
                return
        # Get reactions
        for reaction in msg.reactions:
            if str(reaction.emoji) == self.suggestion_emotes[0]:
                upvote = [user.id async for user in reaction.users()]
                if str(payload.emoji) == self.suggestion_emotes[1]:
                    if payload.member.id in upvote:
                        try:
                            await msg.remove_reaction(str(payload.emoji), payload.member)
                        except:
                            pass
            if str(reaction.emoji) == self.suggestion_emotes[1]:
                downvote = [user.id async for user in reaction.users()]
                if str(payload.emoji) == self.suggestion_emotes[0]:
                    if payload.member.id in downvote:
                        try:
                            await msg.remove_reaction(str(payload.emoji), payload.member)
                        except:
                            pass
        # Trash Can
        if str(payload.emoji) != self.suggestion_emotes[2]:
            return
        if not msg.embeds or not msg.embeds[0].footer or not msg.embeds[0].footer.text:
            try:
                await msg.remove_reaction(str(payload.emoji), payload.member)
            except:
                pass
            return
        result = re.search(r"\([0-9]+\)", msg.embeds[0].footer.text, re.IGNORECASE)
        if not result:
            try:
                await msg.remove_reaction(str(payload.emoji), payload.member)
            except:
                pass
            return
        if f"({payload.member.id})" != result.group(0) and not payload.member.guild_permissions.ban_members:
            try:
                await msg.remove_reaction(str(payload.emoji), payload.member)
            except:
                pass
            return
        await msg.delete()

def setup(bot):
    bot.add_cog(Suggestions(bot))
