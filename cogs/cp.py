import asyncio
import json
import typing
from datetime import datetime, timedelta
from io import BytesIO

import discord
from aiohttp import ClientSession
from discord.ext import commands, tasks

import utils.checks as perms
from utils.CPClientAPI import Client


class ClubPenguinClientApi(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.session = ClientSession()
        self.get_penguin = Client(session=self.session).get_penguin
        self.setup_emotes()
        self.db = self.bot.db["penguins"]
        self._puffle_alarms.start()
        self.guild = self.bot.get_guild(715212041887023204)
        self.puffle_gallery = "https://imgur.com/a/KUKBEPG"
        self.puffle_name_blacklist = ["horny", "blackguy"]

# Module Management~

    def cog_unload(self):
        self._puffle_alarms.cancel()
        asyncio.create_task(self.kill_session())

    def setup_emotes(self):
        self.coin_emote = self.bot.get_emoji(729614260937687042)
        self.puffle_emote = self.bot.get_emoji(729615416938463243)
        self.stamp_emote = self.bot.get_emoji(729615961706987550)
        self.food_emote = self.bot.get_emoji(729597042669649930)
        self.rest_emote = self.bot.get_emoji(729597042363596891)
        self.play_emote = self.bot.get_emoji(729597042497945641)
        self.clean_emote = self.bot.get_emoji(729597042770575430)
        self.loading_emote = self.bot.get_emoji(730799945392586792)
        self.pink_emote = self.bot.get_emoji(717016656202956820)
        self.blue_emote = self.bot.get_emoji(717013472763969616)

    async def kill_session(self):
        await self.session.close()

# Tasks

    @tasks.loop(seconds=600)
    async def _puffle_alarms(self):
        try:
            users = list(self.db.find({"puffle_alarms": True}))
            for user in users:
                if not self.guild:
                    return
                member = self.guild.get_member(user["_id"])
                if not member:
                    continue
                try:
                    data = await self.get_penguin(user["penguin_id"])
                except:
                    continue
                done = user["alarms_done"]
                all_puffle_ids = [puffle.id for puffle in data.puffles]
                for puffle in done:
                    if puffle not in all_puffle_ids:
                        done.remove(puffle)
                for puffle in data.puffles:
                    if puffle.id not in done:
                        e = discord.Embed(description="Your puffle needs attention",
                                          color=discord.Color(puffle.color))
                        e.set_author(name=puffle.name, icon_url=member.avatar_url)
                        if puffle.stats.food < 20:
                            e.add_field(name=f"{self.food_emote} **Food:**",
                                        value=f"{puffle.stats.food}%")
                        if puffle.stats.rest < 20:
                            e.add_field(name=f"{self.rest_emote} **Rest**",
                                        value=f"{puffle.stats.rest}%")
                        if puffle.stats.clean < 20:
                            e.add_field(name=f"{self.clean_emote} **Clean**",
                                        value=f"{puffle.stats.clean}%")
                        if puffle.stats.play < 20:
                            e.add_field(name=f"{self.play_emote} **Play**",
                                        value=f"{puffle.stats.play}%")
                        if len(e.fields) == 4:
                            e.insert_field_at(2, name="\u200b", value="\u200b")
                            e.add_field(name="\u200b", value="\u200b")
                        elif len(e.fields) > 2:
                            e.insert_field_at(2, name="\u200b", value="\u200b")
                        e.set_thumbnail(url=puffle.image_link)
                        if len(e.fields) > 0:
                            try:
                                await member.send(embed=e)
                            except:
                                continue
                            done.append(puffle.id)
                    if puffle.id in done:
                        if (puffle.stats.food > 20 and 
                            puffle.stats.rest > 20 and 
                            puffle.stats.clean > 20 and 
                            puffle.stats.play > 20):
                            done.remove(puffle.id)
                self.db.update_one({"_id": user["_id"]}, {"$set": {"alarms_done": done}})
        except Exception as e:
            print(e.__traceback__.tb_lineno)

# Commands

    @commands.command(name="gangs") 
    @commands.cooldown(1, 60, commands.BucketType.guild)
    async def _gangs(self, ctx):
        pink_gang = self.guild.get_role(717467602552881182)
        blue_gang = self.guild.get_role(718953151625166868)
        e = discord.Embed(description=f"Join a gang in <#717938256439803905>", color=0x000000)
        e.set_author(name="Gangs", icon_url=ctx.guild.icon_url)
        e.add_field(name=f"{self.blue_emote} Blue Gang", value=f"Members: {len(blue_gang.members)}")
        e.add_field(name=f"{self.pink_emote} Pink Gang", value=f"Members: {len(pink_gang.members)}")
        await ctx.send(embed=e)

    @commands.command(name="donators", aliases=["donors"])
    @commands.cooldown(1, 60, commands.BucketType.guild)
    async def _donators(self, ctx):
        role = self.guild.get_role(720420648559575062)
        donators = [f"`{m}`\n" for m in sorted(role.members, key=lambda x: str(x).casefold())]
        text = ""
        for donor in donators:
            text += donor
        e = discord.Embed(description=text, color=role.color)
        e.set_author(name=f"Donators ({len(role.members)})", icon_url=self.guild.icon_url)
        await ctx.send(embed=e)

    @commands.command(name="penguin", aliases=["peng"]) 
    @commands.cooldown(1, 10, commands.BucketType.user)
    async def _cpc(self, ctx, penguin1: typing.Optional[discord.Member]=None, *, penguin=None):
        if not penguin1 and not penguin:
            db_data = self.db.find_one({"_id": ctx.author.id})
            if not db_data:
                e = discord.Embed(description="**Example**\n\n>penguin <penguin>", color=0xffff00)
                e.set_author(name=ctx.command.name, icon_url=self.bot.user.avatar_url)
                await ctx.send(embed=e)
                ctx.command.reset_cooldown(ctx)
                return
            penguin = db_data["penguin_id"]
        if penguin1:
            db_data = self.db.find_one({"_id": penguin1.id})
            if not db_data:
                await ctx.send(f"{penguin1} doesn't have a linked CPC account. Link one doing `>cpcd <penguin Name or ID>`")
                ctx.command.reset_cooldown(ctx)
                return
            penguin = db_data["penguin_id"]
        try:
            data = await self.get_penguin(penguin)
        except Exception as e:
            await ctx.send(f"**{str(e)}**")
            return
        if not data:
            await ctx.send("Please make sure to input the right Penguin Name or ID")
            ctx.command.reset_cooldown(ctx)
            return
        e=discord.Embed(timestamp=data.registered_at,
                        color=0x008000)
        e.set_author(name=data.safe_name)
        ddata = self.db.find_one({"penguin_id": data.id, "verified": True})
        if ddata:
            e.set_author(name=data.name, icon_url=ctx.guild.get_member(ddata["_id"]).avatar_url)
        e.add_field(name="Puffles", value=f'{self.puffle_emote} {len(data.puffles)}')
        e.add_field(name="Coins", value=f'{self.coin_emote} {data.coins}')
        e.add_field(name="Stamps", value=f'{self.stamp_emote} {len(data.stamps)}')
        e.add_field(name="Age", value=f'{self.time(data.age_in_seconds)}')
        e.set_footer(text=f"ID: {data.id} | Registered on")
        if data.avatar:
            image_file=discord.File(data.avatar, filename="image.png")
            e.set_image(url="attachment://image.png")
        else:
            await ctx.send(embed=e)
            return
        await ctx.send(file=image_file, embed=e)

    @commands.command(name="puffles", aliases=["puffle"])
    @commands.cooldown(1, 60, commands.BucketType.user)
    async def _puffles(self, ctx, penguin: typing.Optional[discord.Member]=None, filtering=None):
        if not penguin:
            penguin = ctx.author
        penguini = self.db.find_one({"_id": penguin.id})
        if not penguini:
            await ctx.send(f"**{penguin.display_name}** doesn't have a linked CPC account. Link one doing `>cpcd <penguin Name or ID>`")
            ctx.command.reset_cooldown(ctx)
            return
        if penguini["verified"] == False:
            await ctx.send(f"**{penguin.display_name}** discord link hasn't yet been accepted.")
            ctx.command.reset_cooldown(ctx)
            return
        penguin_id = penguini["penguin_id"]
        try:
            data = await self.get_penguin(penguin_id)
        except Exception as e:
            await ctx.send(f"**{str(e)}**")
            return
        page = 0
        if filtering:
            for puffle in data.puffles:
                if puffle.name.casefold() == filtering.casefold():
                    page = data.puffles.index(puffle)
        if len(data.puffles) == 0:
            await ctx.send(f"**{penguin.display_name}** doesn't own any puffles.")
            return
        msg = await ctx.send(embed=discord.Embed(description=f"{self.loading_emote}"))
        def check(reaction, user):
            return user == ctx.author and reaction.message.id == msg.id and str(reaction.emoji) in ["◀️", "▶️"]
        names = [puffle.name.lower() for puffle in data.puffles]
        def check2(message):
            return message.channel.id == ctx.channel.id and message.author.id == ctx.author.id and message.content.lower() in names
        right = True
        while True:
            if data.puffles[page].name.lower() in self.puffle_name_blacklist and not penguin.guild_permissions.ban_members:
                await ctx.send("Your puffle name is inappropriate so it was skipped.", delete_after=4)
                if right == True:
                    page += 1
                else:
                    page -= 1
            puffle = data.puffles[page]
            e = discord.Embed(description="Tip: Typing name of a puffle within time limit, navigates directly to them.",
                              timestamp=puffle.adopted_at,
                              color=puffle.color)
            e.set_author(name=puffle.name, icon_url=penguin.avatar_url)
            e.add_field(name="Food", value=f"{self.progress_bar(puffle.stats.food, 100, 10)}")
            e.add_field(name="Play", value=f"{self.progress_bar(puffle.stats.play, 100, 10)}")
            e.add_field(name="\u200b", value="\u200b")
            e.add_field(name="Clean", value=f"{self.progress_bar(puffle.stats.clean, 100, 10)}")
            e.add_field(name="Rest", value=f"{self.progress_bar(puffle.stats.rest, 100, 10)}")
            e.add_field(name="\u200b", value="\u200b")
            e.set_thumbnail(url=puffle.image_link)
            e.set_footer(text=f"Puffle {page + 1}/{len(data.puffles)} | Adopted at")
            try:
                await msg.edit(embed=e)
            except:
                pass
            if len(data.puffles) == 1:
                return
            await msg.add_reaction("◀️")
            await msg.add_reaction("▶️")
            done, pending = await asyncio.wait([
                self.bot.wait_for('message', check=check2, timeout=65),
                self.bot.wait_for('reaction_add', check=check, timeout=60)
            ], return_when=asyncio.FIRST_COMPLETED)
            for future in pending:
                future.cancel()
            try:
                stuff = done.pop().result()
            except Exception as e:
                try:
                    await msg.clear_reactions()
                except:
                    break
                break
            if type(stuff) == type(tuple()):
                if str(stuff[0].emoji) == "◀️":
                    right = False
                    page -= 1
                    if page < 0:
                        page = len(data.puffles) - 1
                if str(stuff[0].emoji) == "▶️":
                    right = True
                    page += 1
                    if page > len(data.puffles) - 1:
                        page = 0
                try:
                    await stuff[0].remove(ctx.author)
                except:
                    pass
            else:
                page = names.index(stuff.content.lower())
                try:
                    await stuff.delete()
                except:
                    pass

    @commands.command(name="puffalarms", aliases=["puffle_alarms", "puff_alarms", "alarms"])
    async def __puffle_alarms(self, ctx):
        penguin = self.db.find_one({"_id": ctx.author.id})
        if not penguin:
            await ctx.send(f"You haven't linked a discord account.\nUse `{ctx.prefix}cpcd [penguin name/ID]`")
            return
        if penguin["puffle_alarms"] == False:
            self.db.update_one({"_id": ctx.author.id}, {"$set": {"puffle_alarms": True}})
            await ctx.send(f"{ctx.author.mention}, you have enabled your puffle alarms.\nWhen one of your stats for each puffle falls under 20%, you will be notified in dm's.\n**Make sure you can receive dm's from the bot, you can test this by dm ing the bot if message goes through, you are good.**")
        else:
            self.db.update_one({"_id": ctx.author.id}, {"$set": {"puffle_alarms": False}})
            await ctx.send(f"{ctx.author.mention}, you have disabled your puffle alarms.\nYou'll no longer be notified.")

    @commands.command(name="cpc_discord", aliases=["cpcd"])
    async def _cpc_discord(self, ctx, *, penguin):
        settings = self.bot.db.settings.find_one({"_id": self.bot.user.id})
        if not settings or settings["cpcd_logs"] == None or not self.bot.get_channel(settings["cpcd_logs"]):
            await ctx.send("Uh Oh, this feature is unavailable, ask server admins to set it up!")
            return
        try:
            data = await self.get_penguin(penguin, avatar=False)
        except Exception as e:
            await ctx.send(f"**{str(e)}**")
            return
        if not data:
            await ctx.send("Please make sure to input the right Penguin Name or ID")
            return
        dupe_test = self.db.find_one({"penguin_id": data.id})
        if dupe_test:
            user = await self.bot.fetch_user(dupe_test["_id"])
            await ctx.send(f"**{data.name}** is already linked to {user}")
            return
        entry = self._penguin_default(ctx.author, data)
        try:
            self.db.insert_one(entry)
            await ctx.send(f"You have submitted a discord link for **{data.name}** ({data.id})")
            e = discord.Embed(color=self.bot.progress, timestamp=datetime.utcnow(), description=f"Account link for `{data.name}` **{data.id}**\n\n**Accept** `{ctx.prefix}accept {ctx.author.id}`\n\n**Deny** `{ctx.prefix}deny {ctx.author.id} [reason]`")
            e.set_author(name=ctx.author, icon_url=ctx.author.avatar_url)
            e.set_footer(text="Requested on")
            await self.bot.get_channel(settings["cpcd_logs"]).send(embed=e)
        except:
            user = self.db.find_one({"_id": ctx.author.id})
            if user and user["verified"] == False:
                await ctx.send("Your submition is still waiting verification, please wait.")
            if user and user["verified"] == True:
                await ctx.send("You already have a discord link.")

    @commands.command(name="accept", aliases=["approve"])
    @perms.has_permissions("ban_members")
    async def _accept(self, ctx, member: discord.Member):
        if ctx.guild.id != self.guild.id:
            return
        data = self.db.find_one({"_id": member.id})
        if not data:
            await ctx.send(f"**{member.display_name}** hasn't yet submitted a link request.")
            return
        if data["verified"] == True:
            await ctx.send("That penguin is already verified!")
            return
        self.db.update_one({"_id": member.id}, {"$set": {"verified": True}})
        try:
            await member.send(f"{member.mention} your discord link was approved.")
        except:
            pass
        settings = self.bot.db.settings.find_one({"_id": self.bot.user.id})
        if not settings["cpcd_logs"]:
            return
        channel = self.bot.get_channel(settings["cpcd_logs"])
        if channel:
            async for m in channel.history(limit=1000):
                if m.embeds and str(member.id) in m.embeds[0].description:
                    try:
                        await m.delete()
                    except:
                        pass
                    break
        await ctx.send(f"You accepted <@{member.id}> link for **{data['penguin_id']}**!", delete_after=5)

    @commands.command(name="deny", aliases=["decline"])
    @perms.has_permissions("ban_members")
    async def _deny(self, ctx, member: discord.Member, *, reason="None given"):
        if ctx.guild.id != self.guild.id:
            return
        data = self.db.find_one({"_id": member.id})
        if not data:
            await ctx.send(f"**{member.display_name}** hasn't yet submitted a link request.")
            return
        self.db.delete_one({"_id": member.id})
        try:
            await member.send(f"{member.mention} your discord link was denied.\nReason: {reason}")
        except:
            pass
        settings = self.bot.db.settings.find_one({"_id": self.bot.user.id})
        if not settings["cpcd_logs"]:
            return
        channel = self.bot.get_channel(settings["cpcd_logs"])
        if channel:
            async for m in channel.history(limit=1000):
                if m.embeds and str(member.id) in m.embeds[0].description:
                    try:
                        await m.delete()
                    except:
                        pass
                    break
        await ctx.send(f"You denied <@{member.id}> link for **{data['penguin_id']}**!", delete_after=5)

    @commands.command(name="unlink", aliases=["cpcu"])
    @perms.has_permissions("ban_members")
    async def _unlink(self, ctx, member: discord.User):
        if ctx.guild.id != self.guild.id:
            return
        data = self.db.find_one({"_id": member.id})
        if not data:
            await ctx.send("That user doesn't have a Discord Link.")
            return
        await ctx.send(f"Are you sure you want to remove **{member.display_name}** discord link.")
        def check(message):
            return message.channel.id == ctx.channel.id and message.author.id == ctx.author.id
        try:
            msg = await self.bot.wait_for("message", check=check, timeout=30)
            if msg.content in ["y", "yes"]:
                self.db.delete_one({"_id": member.id})
                await ctx.send(f"**{member.display_name}** discord link was removed.")
                return
            await ctx.send("Discord link was cancelled!")
        except:
            await ctx.send("Timeout discord link cancelled!")
        
    @commands.command(name="cpclog_channel", aliases=["cpclc"])
    @perms.has_permissions("administrator")
    async def _cpcd_log_channel(self, ctx, channel: discord.TextChannel=None):
        if ctx.guild.id != self.guild.id:
            return
        settings = self.bot.db.settings.find_one({"_id": self.bot.user.id})
        if not channel:
            if not settings["cpcd_logs"]:
                await ctx.send("You haven't setup a channel!")
                return
            self.bot.db.settings.update_one({"_id": self.bot.user.id}, {"$set": {"cpcd_logs": None}})
            await ctx.send("Channel reset! New submissions won't be allowed.")
            return
        if settings["cpcd_logs"] == channel.id:
            await ctx.send("That channel is already set as logs channel.")
            return
        self.bot.db.settings.update_one({"_id": self.bot.user.id}, {"$set": {"cpcd_logs": channel.id}})
        await ctx.send(f"{channel.mention} was set as logs channel.")

# Helper Functions

    def _penguin_default(self, member, data):
        entry = {
            "_id": member.id,
            "penguin_id": data.id,
            "description": "CP Client Penguin",
            "puffle_alarms": False,
            "alarms_done": [],
            "verified": False
        }
        return entry

    def progress_bar(self, current, total, size):
        calc = round(current / total * size)
        return f"**{current}%** `﴾" + "=" * calc + " "* (size - calc) + f"﴿`"

    def time(self, timestamp):
        m, s = divmod(timestamp, 60)
        h, m = divmod(m, 60)
        d, h = divmod(h, 24)
        #w, d = divmod(d, 7)
        M, d = divmod(d, 30)
        y, M = divmod(M, 12)
        time = ""
        if y:
            time += f"{int(y)}y "
        if M:
            time += f"{int(M)}M "
        #if w >= 1:
        #    time += f"{int(w)}w "
        if d:
            time += f"{int(d)}d "
        if d < 1 and h:
            time += f"{int(h)}h "
        if h < 1 and m:
            time += f"{int(m)}m "
        if m < 1 and  s:
            time += f"{int(s)}s "
        return time[:-1]

def setup(bot):
    bot.add_cog(ClubPenguinClientApi(bot))
