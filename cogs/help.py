import asyncio
import math

import discord
from discord.ext import commands

class Help(commands.Cog):
    """Help module"""

    def __init__(self, bot):
        self.bot = bot
        self.file = self.bot.db.help

    async def total_commands(self):
        commands = [c["_id"] for c in list(self.file.find({"$or": [{"hidden": False}, {"hidden": {"$exists": False}}]}))]
        for command in list(self.file.find({"subcommands": {"$exists": True}})):
            for subcommand in command["subcommands"]:
                commands.append(f"{command['_id']} {subcommand}")
                if "subcommands" not in command["subcommands"][subcommand]:
                    continue
                for subsubcommand in command["subcommands"][subcommand]["subcommands"]:
                    commands.append(f"{command['_id']} {subcommand} {subsubcommand}")
        return commands

    async def get_description(self, command):
        command = command.split(" ")
        if len(command) == 1:
            command = self.file.find_one({"_id": command[0]})
            return command["description"]
        elif len(command) == 2:
            commandx = self.file.find_one({"_id": command[0]})
            return commandx["subcommands"][command[1]]["description"]
        elif len(command) == 3:
            commandx = self.file.find_one({"_id": command[0]})
            return commandx["subcommands"][command[1]]["subcommands"][command[2]]["description"]

    @commands.command()
    async def help(self, ctx, commandname=None, *, subcommand=None):
        all_commands = await self.total_commands()
        msg = ""
        page = 1
        total_pages = math.ceil(len(all_commands) / 10)
        prefix = ctx.prefix
        if commandname and not subcommand:
            command_db  = self.file.find_one({"_id": commandname})
            if command_db:
                embed=discord.Embed(title=commandname.capitalize(), colour=self.bot.comment)
                embed.add_field(name="Description", value=command_db["description"], inline=False)
                if commandname in self.bot.all_commands:
                    if self.bot.all_commands[commandname].usage:
                        embed.add_field(name="Usage", value=self.bot.all_commands[commandname].usage, inline=False)
                    embed.add_field(name="Example", value=f'``{command_db["example"].replace("{prefix}", prefix)}``')
                    if command_db["aliases"]:
                        embed.add_field(name="Aliases", value=", ".join(command_db["aliases"]), inline=False)
                    if "subcommands" in command_db and command_db["subcommands"]:
                        embed.add_field(name="Subcommands", value=", ".join(list(command_db["subcommands"])), inline=False)
                    await ctx.send(embed=embed)
                else:
                    await ctx.send("Invalid command!")
            elif commandname in self.bot.all_commands:
                command_dbx = self.file.find_one({"_id": str(self.bot.all_commands[commandname])})
                if command_dbx:
                    commandname = str(self.bot.all_commands[commandname])
                    embed=discord.Embed(title=commandname.capitalize(), colour=self.bot.comment)
                    embed.add_field(name="Description", value=command_dbx["description"], inline=False)
                    if self.bot.all_commands[commandname].usage:
                        embed.add_field(name="Usage", value=self.bot.all_commands[commandname].usage, inline=False)
                    embed.add_field(name="Example", value=f'``{command_dbx["example"].replace("{prefix}", prefix)}``')
                    if command_dbx["aliases"]:
                        embed.add_field(name="Aliases", value=", ".join(command_dbx["aliases"]), inline=False)

                    if "subcommands" in command_dbx and command_dbx["subcommands"]:
                        embed.add_field(name="Subcommands", value=", ".join(list(command_dbx["subcommands"])), inline=False)
                    await ctx.send(embed=embed)
            else:
                await ctx.send("Invalid command!")
        elif commandname and subcommand or commandname in self.bot.all_commands.keys():
            if self.file.find_one({"_id": commandname}) or self.file.find_one({"_id": str(self.bot.all_commands[commandname])}):
                commandname = str(self.bot.all_commands[commandname])
                command_db = self.file.find_one({"_id": commandname})
                subcommands = subcommand.split(" ")
                try:
                    if len(subcommands) == 1:
                        command_name = self.bot.all_commands[commandname].all_commands[subcommands[0]]
                        command_path = command_db["subcommands"][subcommands[0]]
                        description = command_path["description"]
                        example = command_path["example"].replace("{prefix}", prefix)
                    elif len(subcommands) == 2:
                        command_name = self.bot.all_commands[commandname].all_commands[subcommands[0]].all_commands[subcommands[1]]
                        command_path= command_db["subcommands"][subcommands[0]]["subcommands"][subcommands[1]]
                        description = command_path["description"]
                        example = command_path["example"].replace("{prefix}", prefix)
                    elif len(subcommands) == 3:
                        command_name = self.bot.all_commands[commandname].all_commands[subcommands[0]].all_commands[subcommands[1]].all_commands[subcommands[2]]
                        command_path = command_db["subcommands"][subcommands[0]]["subcommands"][subcommands[1]]["subcommands"][subcommands[2]]
                        description = command_path["description"]
                        example = command_path["example"].replace("{prefix}", prefix)
                    elif len(subcommands) > 3:
                        await ctx.send("Invalid subcommand!")
                        return
                except KeyError:
                    await ctx.send("Invalid subcommand!")
                    return
                embed=discord.Embed(title=str(command_name).capitalize(), colour=self.bot.comment)

                embed.add_field(name="Description", value=description, inline=False)
                if command_name.usage:
                    embed.add_field(name="Usage", value=command_name.usage, inline=False)
                embed.add_field(name="Example", value=f'``{example}``')
                if command_path["aliases"]:
                    embed.add_field(name="Aliases", value=", ".join(command_path["aliases"]), inline=False)

                if "subcommands" in command_path and command_path["subcommands"]:
                    embed.add_field(name="Subcommands", value=", ".join(list(command_path["subcommands"])), inline=False)
                await ctx.send(embed=embed)
        elif not commandname and not subcommand:
            for command in all_commands[:10]:
                description = await self.get_description(command)
                command_path = command
                msg += f'``{prefix}{command_path}`` - {description}\n\n'
            embed=discord.Embed(title=f"Page: {page}/{total_pages}",description=msg, colour=self.bot.comment)
            embed.set_author(name="Commands", icon_url=self.bot.user.avatar_url)
            embed.set_footer(text=f"{prefix}help <command>/<subcommand> for more info on the command.")
            message = await ctx.send(embed=embed)
            if total_pages == 1:
                return
            await message.add_reaction("◀")
            await message.add_reaction("▶")
            def reactioncheck(reaction, user):
                if user != self.bot.user:
                    if user == ctx.author:
                        if reaction.message.channel == ctx.channel:
                            if reaction.emoji == "▶" or reaction.emoji == "◀":
                                if reaction.message.id == message.id:
                                    return True
            page2 = True
            while page2:
                try:
                    reaction, author = await self.bot.wait_for("reaction_add", timeout=120, check=reactioncheck)
                    if reaction.emoji == "▶":
                        if page != total_pages:
                            page += 1
                            msg = ""
                            for command in all_commands[page*10-10:page*10]:
                                description = await self.get_description(command)
                                command_path = command
                                msg += f'``{prefix}{command_path}`` - {description}\n\n'
                            embed=discord.Embed(title=f"Page: {page}/{total_pages}",description=msg, colour=self.bot.comment)
                            embed.set_author(name="Commands", icon_url=self.bot.user.avatar_url)
                            embed.set_footer(text=f"{prefix}help <command>/<subcommand> for more info on the command.")
                            await message.edit(embed=embed)
                            await message.remove_reaction("▶", ctx.author)
                        else:
                            page = 1
                            msg = ""
                            for command in all_commands[page*10-10:page*10]:
                                description = await self.get_description(command)
                                command_path = command
                                msg += f'``{prefix}{command_path}`` - {description}\n\n'
                            embed=discord.Embed(title=f"Page: {page}/{total_pages}",description=msg, colour=self.bot.comment)
                            embed.set_author(name="Commands", icon_url=self.bot.user.avatar_url)
                            embed.set_footer(text=f"{prefix}help <command>/<subcommand> for more info on the command.")
                            await message.edit(embed=embed)
                            await message.remove_reaction("▶", ctx.author)
                    elif reaction.emoji == "◀":
                        if page != 1:
                            page -= 1
                            msg = ""
                            for command in all_commands[page*10-10:page*10]:
                                description = await self.get_description(command)
                                command_path = command
                                msg += f'``{prefix}{command_path}`` - {description}\n\n'
                            embed=discord.Embed(title=f"Page: {page}/{total_pages}",description=msg, colour=self.bot.comment)
                            embed.set_author(name="Commands", icon_url=self.bot.user.avatar_url)
                            embed.set_footer(text=f"{prefix}help <command>/<subcommand> for more info on the command.")
                            await message.edit(embed=embed)
                            await message.remove_reaction("◀", ctx.author)
                        else:
                            page = total_pages
                            msg = ""
                            for command in all_commands[page*10-10:page*10]:
                                description = await self.get_description(command)
                                command_path = command
                                msg += f'``{prefix}{command_path}`` - {description}\n\n'
                            embed=discord.Embed(title=f"Page: {page}/{total_pages}",description=msg, colour=self.bot.comment)
                            embed.set_author(name="Commands", icon_url=self.bot.user.avatar_url)
                            embed.set_footer(text=f"{prefix}help <command>/<subcommand> for more info on the command.")
                            await message.edit(embed=embed)
                            await message.remove_reaction("◀", ctx.author)
                except asyncio.TimeoutError:
                    try:
                        await message.clear_reactions()
                    except:
                        pass
                    page2 = False

def setup(bot):
    n = Help(bot)
    bot.remove_command('help')
    bot.add_cog(n)
